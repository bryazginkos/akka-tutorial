package actors;

import akka.actor.UntypedActor;
import messages.ReportMessage;
import messages.TaskMessage;

/**
 * Created by kos on 29.06.15.
 */
public class PlusActor extends UntypedActor {

    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof TaskMessage) {
            TaskMessage taskMessage = (TaskMessage) message;
            int a = taskMessage.getA();
            int b = taskMessage.getB();
            int r = a + b;
            ReportMessage reportMessage = new ReportMessage(r);
            getSender().tell(reportMessage, getSelf());
        }
    }
}
