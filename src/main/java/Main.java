import actors.ChiefActor;
import actors.PlusActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import messages.TaskMessage;

/**
 * Created by kos on 29.06.15.
 */
public class Main {
    public static void main(String[] args) {
        final ActorSystem system = ActorSystem.create("helloakka");
        final ActorRef plusActor = system.actorOf(Props.create(PlusActor.class), "plus");
        final ActorRef chiefActor = system.actorOf(Props.create(ChiefActor.class, plusActor), "chief");
        chiefActor.tell(new TaskMessage(1 ,3), ActorRef.noSender());
    }
}
