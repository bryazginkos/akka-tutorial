package actors;

import akka.actor.*;
import messages.ReportMessage;
import messages.TaskMessage;

/**
 * Created by kos on 29.06.15.
 */
public class ChiefActor extends UntypedActor {

    private ActorRef plusActor;

    public ChiefActor(ActorRef plusActor) {
        this.plusActor = plusActor;
    }

    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof TaskMessage) {
            plusActor.tell(message, getSelf());
        }
        if (message instanceof ReportMessage) {
            ReportMessage reportMessage = (ReportMessage) message;
            System.out.println(reportMessage.getResult());
        }
    }


}
