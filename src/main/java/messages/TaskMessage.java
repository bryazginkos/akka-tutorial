package messages;

/**
 * Created by kos on 29.06.15.
 */
public class TaskMessage {
    private int a;
    private int b;

    public TaskMessage(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public int getA() {
        return a;
    }

    public int getB() {
        return b;
    }
}
