package messages;

import java.io.Serializable;

/**
 * Created by kos on 29.06.15.
 */
public class ReportMessage implements Serializable {
    private int result;

    public ReportMessage(int result) {
        this.result = result;
    }

    public int getResult() {
        return result;
    }
}
